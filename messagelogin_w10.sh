#!/bin/bash

check_lolcat=$(dpkg -l | egrep -o -m 1 "\blolcat\b" | wc -l)
check_ansiweather=$(dpkg -l | egrep -o -m 1 "\bansiweather\b" | wc -l)
check_lsb_release=$(dpkg -l | egrep -o -m 1 "\blsb-release\b" | wc -l)
check_awk=$(dpkg -l | egrep -o -m 1 "awk" | wc -l)
check_figlet=$(dpkg -l | egrep -o -m 1 "\bfiglet\b" | wc -l )
check_curl=$(dpkg -l | egrep -o -m 1 "\bcurl\b" | wc -l )
privateip=$(hostname -I | head -c 41)

if [ $check_lolcat -eq 0 ] || [ $check_ansiweather -eq 0 ] || [ $check_lsb_release -eq 0 ] || [ $check_awk -eq 0 ] || [ $check_figlet -eq 0 ] || [ $check_curl -eq 0 ]
then
	if [[ $EUID -ne 0 ]]
        then
               	echo "Mise à jour du cache avec sudo :"
               	sudo apt update
        else
               	printf "Mise à jour du cache :"
               	apt update
	fi
fi


if [ $check_lolcat -eq 0 ]
then
	if [[ $EUID -ne 0 ]]
	then
   		echo "Installation avec sudo de lolcat :"
   		sudo apt install -y lolcat
	else
		printf "Téléchargement de lolcat :\n"
		apt install -y lolcat
	fi
fi
if [ $check_ansiweather -eq 0 ]
then
	if [[ $EUID -ne 0 ]]
        then
                echo "Installation avec sudo de ansiweather :"
                sudo apt install -y ansiweather
        else
                printf "Téléchargement de ansiweather :\n"
                apt install -y ansiweather
        fi
fi
if [ $check_lsb_release -eq 0 ]
then
        if [[ $EUID -ne 0 ]]
        then
                echo "Installation avec sudo de lsb-release :"
                sudo apt install -y lsb-release
        else
                printf "Téléchargement de lsb-release :\n"
                apt install -y lsb-release
        fi
fi
if [ $check_awk -eq 0 ]
then
        if [[ $EUID -ne 0 ]]
        then
                echo "Installation avec sudo de awk :"
                sudo apt install -y awk
        else
                printf "Téléchargement de awk :\n"
                apt install -y awk
	fi
fi
if [ $check_figlet -eq 0 ]
then
        if [[ $EUID -ne 0 ]]
        then
                echo "Installation avec sudo de figlet :"
                sudo apt install -y figlet
        else
                printf "Téléchargement de figlet :\n"
                apt install -y figlet
        fi
fi
if [ $check_curl -eq 0 ]
then
        if [[ $EUID -ne 0 ]]
        then
                echo "Installation avec sudo de curl :"
                sudo apt install -y curl
        else
                printf "Téléchargement de curl :\n"
                apt install -y curl
        fi
fi

figlet -f smslant -w 300 $(hostname) | /usr/games/lolcat

cpuallusage=`ps aux | awk {'sum+=$3;print sum'} | tail -n 1 | awk '{printf "%.0f\n", $1}'`
cpucores=`cat /proc/cpuinfo | grep "model name" | wc -l`
cpuusage=$((cpuallusage / cpucores))

# \033[0;34m+     \033[0;37mCPU usage \033[0;34m= \033[1;32m$cpuusage%
echo $(ansiweather -l blasimon)
echo -e "\033[0;34m++++++++++++++++++++++++: \033[0;37mSystem Data\033[0;34m :++++++++++++++++++++++++
   \033[0;37mDate and Time \033[0;34m= \033[1;32m`date`\033[0;34m
   \033[0;37mKernel \033[0;34m= \033[1;32m`uname -r`
   \033[0;37mOS \033[0;34m= \033[1;32m`lsb_release -d | sed "s/Description://g;s/[[:blank:]]//g"`
   \033[0;37mUptime \033[0;34m= \033[1;32m`uptime | grep -ohe 'up .*' | sed 's/,//g' | awk '{ printf $2" "$3 }'`
   \033[0;37mLoad \033[0;34m= \033[1;32m`cat /proc/loadavg | awk '{print $1}'` `cat /proc/loadavg | awk '{print $2}'` `cat  /proc/loadavg | awk '{print $3}'`
   \033[0;37mCPU model \033[0;34m= \033[1;32m$cpucores x `cat /proc/cpuinfo | grep -m 1 "model name" | awk -F ": " '{print $2}'`
   \033[0;37mCPU usage \033[0;34m= \033[1;32m$cpuusage%
   \033[0;37mMemory \033[0;34m= \033[1;32mUsed `free -m | head -n 3 | tail -n 1 | awk {'print $3'}` MB out of `free -m | head -n 2 | tail -n 1 | awk {'print $2'}` MB
   \033[0;37mSSD / \033[0;34m= \033[1;32mUsed `df -h / | tail -n 1 | awk {'print $3'}` out of `df -h / | tail -n 1 | awk {'print $2'}`
   \033[0;37mSSD commun \033[0;34m= \033[1;32mUsed `df -h /mnt/g | tail -n 1 | awk {'print $3'}` out of `df -h /mnt/g | tail -n 1 | awk {'print $2'}`
   \033[0;37mProcesses \033[0;34m= \033[1;32m`ps aux | wc -l` of `ulimit -u` MAX
   \033[0;37mPublic IP \033[0;34m= \033[1;32m`curl -s ifconfig.me`
   \033[0;37mPrivate IP \033[0;34m= \033[1;32m$privateip
\033[0;34m++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[0;37m"


